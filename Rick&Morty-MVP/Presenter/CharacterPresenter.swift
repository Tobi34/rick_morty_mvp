//
//  CharacterPresenter.swift
//  Rick&Morty-MVP
//
//  Created by tobi adegoroye on 05/09/2021.
//

import Foundation

class CharacterPresenter {
    
    private let networkService = NetworkService()
    weak private var characterViewDelegate: CharacterViewDelegate?
    
    func setViewDelegate(characterViewDelegate:CharacterViewDelegate) {
        self.characterViewDelegate = characterViewDelegate
    }
    
    func getAllCharacters(){
        networkService.getCharacter { res in
            switch res {
            case .success(let character):
                self.characterViewDelegate?.display(character: character)
            case .failure(let error):
                print(error)
            }
        }
    }
}
