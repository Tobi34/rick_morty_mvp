//
//  Protocol.swift
//  Rick&Morty-MVP
//
//  Created by tobi adegoroye on 05/09/2021.
//

import Foundation

protocol CharacterViewDelegate: AnyObject {
    func display(character: [Character])
}
