//
//  NetworkService.swift
//  Rick&Morty-MVP
//
//  Created by tobi adegoroye on 05/09/2021.
//

import Foundation

class NetworkService {
    
    private let urlString = "https://rickandmortyapi.com/api/character"
    
    func getCharacter(completion: @escaping (Result<[Character], Error>) -> ()){
        guard let url = URL(string: urlString) else {return}
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, _, error in
            
            DispatchQueue.main.async {
                guard let data = data, error == nil else {
                    return
                }
                do {
                    let characterResult = try JSONDecoder().decode(CharacterResult.self, from: data)
                    completion(.success(characterResult.results))
                } catch let jsonError{
                    completion(.failure(jsonError))
                }
            }
        }
        task.resume()
    }
}

