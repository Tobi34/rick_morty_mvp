//
//  Character.swift
//  Rick&Morty-MVP
//
//  Created by tobi adegoroye on 05/09/2021.
//

import Foundation

struct CharacterResult: Codable {
    let info: Info
    let results: [Character]

}

struct Info: Codable {
    let count, pages: Int
    let next: String
 }

struct Character: Codable {
    let id: Int
    let name, status, species, type: String
    let gender: String
    let origin, location: Location
    let image: String
    let episode: [String]
    let url: String
    let created: String
}

struct Location: Codable {
    let name: String
    let url: String
}
