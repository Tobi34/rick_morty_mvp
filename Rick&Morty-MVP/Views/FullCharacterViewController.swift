//
//  FullCharacterViewController.swift
//  Rick-And-Morty-MVP
//
//  Created by tobi adegoroye on 04/09/2021.
//

import UIKit
import Kingfisher

class FullCharacterViewController: UIViewController {

     var character: Character?
    
    @IBOutlet weak var speciesLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var originLbl: UILabel!
    
    @IBOutlet weak var typeLbl: UILabel!
    
    @IBOutlet weak var characterImg: UIImageView!
    
    func setupView() {
        statusLbl.text = character?.status
        speciesLbl.text = character?.species
        typeLbl.text = character?.type
        genderLbl.text = character?.gender
        originLbl.text = character?.origin.name
        characterImg.kf.indicatorType = .activity
        characterImg.kf.setImage(with: URL(string: character?.image ?? ""))
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        title = character?.name
     }
}
 
