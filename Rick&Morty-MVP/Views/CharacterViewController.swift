//
//  ViewController.swift
//  Rick&Morty-MVP
//
//  Created by tobi adegoroye on 05/09/2021.
//

import UIKit

class CharacterViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var characters = [Character]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    private let characterPresenter = CharacterPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        characterPresenter.setViewDelegate(characterViewDelegate: self)
        characterPresenter.getAllCharacters()
    }


}



extension CharacterViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = characters[indexPath.row].name
        cell?.selectionStyle = .none
        return cell!
        
    }
    
    
}

extension CharacterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "FullCharacterViewController") as? FullCharacterViewController {
            vc.character = characters[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension CharacterViewController: CharacterViewDelegate {
    func display(character: [Character]) {
        self.characters = character
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
    }
    
    
}
